# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python3_{4,5} pypy3 )

inherit distutils-r1

DESCRIPTION="A command line utility for Weblate, translation tool with VCS integration"
HOMEPAGE="https://weblate.org/"
SRC_URI="https://github.com/WeblateOrg/wlc/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~sparc ~x86"
IUSE="doc"

DEPEND="dev-python/pyxdg"
RDEPEND="${DEPEND}"

python_install_all() {
	use doc && DOCS=( README.rst )

	distutils-r1_python_install_all
}
